FlightHub-Test Installation
=============================

![docker containers](https://img.shields.io/badge/Docker-Nginx%201.17.4%20%20%7C%20PHP%207.2%20%7C%20%20MYSQL%205.6.19%20%7C%20-brightgreen.svg)


This document explain how to getting started with FlightHub-Test development environment. (Docker, Symfony, etc)  

DEVELOPMENT ENVIRONMENT
-----------------------

The FlightHub-Test use Docker to quickly deploy a development environment. The docker-compose.yml is configure to deploy 3 containers :

* Nginx 1.17.4 [front]
* Php 7.2 [engine]
* Mysql 5.6.19 [db]

INSTALLATION
------------

On suppose you have already Docker installed and running.

### Docker

* `cp docker-compose.yml.dist docker-compose.yml`, customize it with your needs.
* `cp .env.dist .env`, customize it with your needs.
* `docker-compose up -d`

### Symfony

* `docker-compose exec engine php composer.phar install` in root directory of project to install vendor bundles.
* `docker-compose exec db bash docker-entrypoint-initdb.d/init.sql` to add user privileges to the database.
* `docker-compose exec engine php bin/console doctrine:database:create` to create database.
* `docker-compose exec engine php bin/console doctrine:schema:update --force` to generate database structure.
* `docker-compose exec engine php bin/console doctrine:fixtures:load` to populate database with test data.

### Register hosts

Write hosts below to your file `/etc/hosts` : 
```
127.0.0.1 dev.flighthub.com
```

TASK
------------

To see the task open 

```
http://dev.flighthub.com:8080/doc/Trip_Builder.pdf
```

SOLUTION
------------

To do this task I am using Docker with 
* Nginx 1.17.4 [front]
* Php 7.2 [engine]
* Mysql 5.6 [db]
* Symfony 4.3.4

On this step the database was already populated with some data.
To make different trips mentioned in the task request I've created a TripController

### TripControler
* Contains entry points for every trip type
    - "airline_code" in any trip is optional. If exist, only the flights with this specific airline will be searched
    
    - One-way trip
```
        http://dev.flighthub.com:8080/api/trips/one-way
        METHOD : POST
        Body : 
            {
            	"departure_location": "Montreal",
            	"departure_date": "2020-01-20",
            	"arrival_location": "Vancouver",
                "airline_code": "AF"
            }
```
   - Round trip      
```
        http://dev.flighthub.com:8080/api/trips/round
        METHOD : POST
        Body : 
            {
            	"departure_location": "Montreal",
            	"departure_date": "2020-01-20",
            	"arrival_location": "Vancouver",
            	"return_date": "2020-01-20",
            	"airline_code": "AF"
            }
```
   - Open-jaw trip      
```
        http://dev.flighthub.com:8080/api/trips/open-jaw
        METHOD : POST
        Body : 
            {
                "departure_location" : "Montreal",
                "arrival_location" : "Vancouver",
                "middle_location" : "Toronto",
                "departure_date" : "2020-01-10",
                "return_date" : "2020-01-11",
                "airline_code": "AF"
            }
```
   - Multi-city trip      
```
        http://dev.flighthub.com:8080/api/trips/multi-city
        METHOD : POST
        Body : 
            {
            	"flights" :
            	[
            		{
            			"departure_location" : "Montreal",
            			"arrival_location" : "Vancouver",
            			"departure_date" : "2020-01-10",
            			"airline_code": "AF"
            		},
            		{
            			"departure_location" : "Vancouver",
            			"arrival_location" : "Toronto",
            			"departure_date" : "2020-01-11"
            			
            		},
            		{
            			"departure_location" : "Toronto",
            			"arrival_location" : "Calgary",
            			"departure_date" : "2020-01-12"
            		},
            		{
            			"departure_location" : "Calgary",
            			"arrival_location" : "Quebec",
            			"departure_date" : "2020-01-13"
            		},
            		{
            			"departure_location" : "Quebec",
            			"arrival_location" : "Vancouver",
            			"departure_date" : "2020-01-14"
            		}
            	]
            				
            }
```
      
* To keep the controller clean, TripController convert the json data parameters to an array and send it to the TripFactory which creates the trips
* Receive the returned Trips and send them to the Paginator service to be displayed on multiple pages  

### TripFactory
- Creates all different trips with the help of the FlightFactory and return them to the TripController

### FlightFactory
- Reads all possible flights for the trip from the database
- Set the timezones using the DateTimeService
- Validate the creation time and the 365 days period using the DateTimeService  

BONUS
------------  
After execution of the `docker-compose exec engine php bin/console doctrine:fixtures:load` the database will be populated with some 
static data from the AppFixtures.php but I also created CRUDS for Airline, Airport and Flight so the new data for this entities can be added
or these entities can be read by using api end points.
The specific here is that not everyone can read or modify these CRUDS. I have added an Authorization Token which should be added in the client Header request.

So, for these CRUDS requests add in the client Header:
```
Content-Type : application/json
Authorization : Basic lfuhgvblhgkGHJKytfjkdfghdg651rthert615UYGJgyvjftjh
```

### Airline

- Read all airlines
```
    http://dev.flighthub.com:8080/api/airlines
    METHOD : GET
``` 
- Read airline by id
```
    http://dev.flighthub.com:8080/api/airlines/{id}
    METHOD : GET
``` 
- Delete airline by id
```
    http://dev.flighthub.com:8080/api/airlines/{id}
    METHOD : DELETE
``` 
- Create airline
```
    http://dev.flighthub.com:8080/api/airlines
    METHOD : POST
    Body :
    {
        "code": "KM",
        "name": "Air Malta"
    }
```  
- Update airline
```
    http://dev.flighthub.com:8080/api/airlines/{id}
    METHOD : PUT
    Body :
    {
        "code": "KM",
        "name": "Air Malta"
    }
``` 

### Airport

- Read all airports
```
    http://dev.flighthub.com:8080/api/airports
    METHOD : GET
``` 
- Read airport by id
```
    http://dev.flighthub.com:8080/api/airports/{id}
    METHOD : GET
``` 
- Delete airport by id
```
    http://dev.flighthub.com:8080/api/airports/{id}
    METHOD : DELETE
``` 
- Create airport
```
    http://dev.flighthub.com:8080/api/airports
    METHOD : POST
    Body :
    {
        "code": "YUL",
        "city_code": "YMQ",
        "name": "Pierre Elliott Trudeau International",
        "city": "Montreal",
        "country_code": "CA",
        "region_code": "QC",
        "latitude": 45.457714,
        "longitude": -73.749908,
        "timezone": "America/Montreal"
    }
```  
- Update airport
```
    http://dev.flighthub.com:8080/api/airports/{id}
    METHOD : PUT
    Body :
    {
        "code": "YUL",
        "city_code": "YMQ",
        "name": "Pierre Elliott Trudeau International",
        "city": "Montreal",
        "country_code": "CA",
        "region_code": "QC",
        "latitude": 45.457714,
        "longitude": -73.749908,
        "timezone": "America/Montreal"
    }
``` 

### Flight

- Read all flights
```
    http://dev.flighthub.com:8080/api/flights
    METHOD : GET
``` 
- Read flight by id
```
    http://dev.flighthub.com:8080/api/flights/{id}
    METHOD : GET
``` 
- Delete flight by id
```
    http://dev.flighthub.com:8080/api/flights/{id}
    METHOD : DELETE
``` 
- Create flight
```
    http://dev.flighthub.com:8080/api/flights
    METHOD : POST
    Body :
    {
        "airline": "AC",
        "number": "301",
        "departure_airport": "YUL",
        "departure_time": "07:35",
        "arrival_airport": "YVR",
        "arrival_time": "10:05",
        "price": "273.23"
    }
```  
- Update flight
```
    http://dev.flighthub.com:8080/api/flights/{id}
    METHOD : PUT
    Body :
    {
        "airline": "AC",
        "number": "301",
        "departure_airport": "YUL",
        "departure_time": "07:35",
        "arrival_airport": "YVR",
        "arrival_time": "10:05",
        "price": "273.23"
    }
```   
                              
### If given more time and resources
- Because of the limited time I didn't check for many Exceptions and uniqueness of the data in the database, also didn't 
make much validation of the accuracy of the entry data !     