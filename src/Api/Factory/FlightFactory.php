<?php

namespace App\Api\Factory;

use App\Api\Entity\Flight;
use App\Api\Repository\AirlineRepository;
use App\Api\Repository\AirportRepository;
use App\Api\Repository\FlightRepository;
use App\Api\Service\DateTimeService;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class FlightFactory
 * @package App\Api\Factory
 */
class FlightFactory
{
	private $flightRepository;
	private $airportRepository;
	private $airlineRepository;
	private $dateTimeService;

    /**
     * FlightFactory constructor.
     * @param FlightRepository $flightRepository
     * @param AirportRepository $airportRepository
     * @param AirlineRepository $airlineRepository
     * @param DateTimeService $dateTimeService
     */
	public function __construct(FlightRepository $flightRepository, AirportRepository $airportRepository, AirlineRepository $airlineRepository, DateTimeService $dateTimeService)
    {
		$this->flightRepository = $flightRepository;
		$this->airportRepository = $airportRepository;
		$this->airlineRepository = $airlineRepository;
		$this->dateTimeService = $dateTimeService;
    }

    /**
     * @param string $depLoc
     * @param string $arrLoc
     * @param string $flightDate
     * @param string|null $airlineCode
     * @return array
     */
    public function getFlights(string $depLoc, string $arrLoc, string $flightDate, ?string $airlineCode = null)
    {
        $airline = null;
        if ($airlineCode !== null){
            $airline = $this->airlineRepository->findOneBy(['code' => $airlineCode]);
        }

        $dep_loc = $this->airportRepository->findOneBy(['city' => $depLoc]);
        $arr_loc = $this->airportRepository->findOneBy(['city' => $arrLoc]);

        $flightCriteria = ['departureAirport' => $dep_loc,
                           'arrivalAirport' => $arr_loc
        ];

        if ($airline !== null){
            $flightCriteria['airline'] = $airline;
        }

        $flights = $this->flightRepository->findBy($flightCriteria);

        $possibleFlights = [];
        foreach ($flights as $flight)
        {
            $flight->setDepartureTime($this->dateTimeService->modifyDate($flight->getDepartureTime(), $flightDate , $flight->getDepartureAirport()->getTimezone()));

            if (!$this->dateTimeService->isInPeriodLimits($flight)){
                continue;
            }

            $flight->setArrivalTime($this->dateTimeService->modifyDate($flight->getArrivalTime(), $flightDate, $flight->getArrivalAirport()->getTimezone() ));
            $possibleFlights[] = $flight;
        }

		return $possibleFlights;
    }
}