<?php

namespace App\Api\Factory;

use App\Api\Entity\Trip;
use App\Api\Entity\Flight;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class TripFactory
 * @package App\Api\Factory
 */
class TripFactory
{
	private $flightFactory;

    /**
     * TripFactory constructor.
     * @param FlightFactory $flightFactory
     */
	public function __construct(FlightFactory $flightFactory)
    {
		$this->flightFactory = $flightFactory;
    }

    /**
     * @param array $data
     * @return array
     */
    public function createOneWayTrip(array $data)
    {
        $airlineCode = (isset($data['airline_code'])) ? $data['airline_code'] : null;
		$flights = $this->flightFactory->getFlights($data['departure_location'],$data['arrival_location'], $data['departure_date'], $airlineCode);

		$trips = [];
		foreach($flights as $flight){
            $trips[] = $this->createTrip(array($flight));
		}
		return $trips;
    }

    /**
     * @param array $data
     * @return array
     */
	public function createRoundTrip(array $data)
	{
	    $airlineCode = (isset($data['airline_code'])) ? $data['airline_code'] : null;

	    $flights = $this->flightFactory->getFlights($data['departure_location'], $data['arrival_location'], $data['departure_date'], $airlineCode);
		$backFlights = $this->flightFactory->getFlights($data['arrival_location'], $data['departure_location'], $data['return_date'], $airlineCode);

        $allFlights[] = $flights;
        $allFlights[] = $backFlights;

        return $this->getTrips($allFlights);
	}

    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
	public function createOpenJawTrip(array $data)
	{
        $airlineCode = (isset($data['airline_code'])) ? $data['airline_code'] : null;
		$flights = $this->flightFactory->getFlights($data['departure_location'], $data['arrival_location'], $data['departure_date'], $airlineCode);
		$secondFlights = $this->flightFactory->getFlights($data['middle_location'], $data['departure_location'], $data['return_date'], $airlineCode);

        $allFlights[] = $flights;
        $allFlights[] = $secondFlights;

        return $this->getTrips($allFlights);
	}

    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
	public function createMultiCityTrip(array $data)
	{
		$flights = [];
		$reqFlights = $data['flights'];
		foreach ($reqFlights as $data){
            $airlineCode = (isset($data['airline_code'])) ? $data['airline_code'] : null;
			$flights[] = $this->flightFactory->getFlights($data['departure_location'],$data['arrival_location'], $data['departure_date'], $airlineCode);
		}

        return $this->getTrips($flights);
	}

    /**
     * @param array $flights
     * @return array
     * @throws \Exception
     */
    private function getTrips(array $flights)
    {
        $trips = [];
        $allTrips = [];
        $allTrips = $this->getAllTrips($flights, $trips, $allTrips);

        $allFilteredTrips = [];
        $now = new \DateTime();
        foreach($allTrips as $flightsInTrip)
        {
            $nowDate = $now;
            $timeZoneSet = false;
            foreach($flightsInTrip as $flight)
            {
                if (!$timeZoneSet){
                    $timeZone = new \DateTimeZone($flight->getDepartureAirport()->getTimezone());
                    $nowDate->setTimezone($timeZone);
                    $timeZoneSet = true;
                }

                if ($nowDate > $flight->getDepartureTime()){
                    $flightsInTrip = null;
                    break;
                }

                $nowDate = $flight->getArrivalTime();
            }
            if ($flightsInTrip !== null) {
                $allFilteredTrips[] = $flightsInTrip;
            }
        }

        $trips = [];
        foreach($allFilteredTrips as $flightsInTrip){
            $trips[] = $this->createTrip($flightsInTrip);
        }
        return $trips;
    }

    /**
     * @param array $allFlights
     * @param array $trips
     * @param $allTrips
     * @return array
     */
    private function getAllTrips(array $allFlights, array $trips, $allTrips)
	{
		$i = count($allFlights);
		if ($i > 0 ){
			$flights = array_splice($allFlights,0,1)[0];
		}
			
		foreach($flights as $flight){
			if ($i > 1){
				$trips[] = $flight;
				$allTrips = $this->getAllTrips($allFlights, $trips, $allTrips);
				array_pop($trips);
			}
		    else{
				$allTrips[] = array_merge($trips,array($flight));
			}
		}
        return $allTrips;	
	}

    /**
     * @param array $flights
     * @return Trip
     */
	private function createTrip(array $flights){
		$trip = new Trip();
		$totalPrice = 0;
		foreach ($flights as $flight)
		{
			$trip->addFlight($flight);
		  	$totalPrice += $flight->getPrice();
		}
		$trip->setTotalPrice($totalPrice);
		return $trip;
	}
}