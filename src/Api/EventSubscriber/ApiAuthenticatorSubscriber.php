<?php

namespace App\Api\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ApiAuthenticatorSubscriber implements EventSubscriberInterface
{
    protected $controllers;
    protected $token;

    /**
     * ApiAuthenticatorSubscriber constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->controllers = $config['controllers'];
        $this->token = $config['token'];
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => [
                ['checkAuthorizationToken']
            ]
        ];
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function checkAuthorizationToken(FilterControllerEvent $event)
    {
        if (!$event->isMasterRequest() || !is_array($event->getController())) {
            return;
        }

        list($controller) = $event->getController();

        if(!$this->isAuthenticatedController($controller)) {
            return;
        }

        $token = $event->getRequest()->headers->get('Authorization');

        if(is_null($token) || !$this->isAuthorized($token)){
            throw new UnauthorizedHttpException('');
        }
    }

    private function isAuthenticatedController($controller): bool
    {
        return in_array(get_class($controller), $this->controllers);
    }

    private function isAuthorized(?string $token): bool
    {
        return $this->token === $token;
    }

}