<?php

namespace App\Api\Controller;

use App\Api\Entity\Flight;
use App\Api\Form\FlightType;
use App\Api\Factory\TripFactory;
use App\Api\Form\OneWayTripType;
use App\Api\Form\OpenJawTripType;
use App\Api\Form\RoundTripType;
use App\Api\Service\Paginator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as REST;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Hateoas\Configuration\Route;
use Hateoas\Representation\PaginatedRepresentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter as PARAMCONVERTER;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


/**
 * Class TripController
 * @package App\Api\Controller
 */
class TripController extends FOSRestController
{
    /**
     * @param Request $request
     * @param Paginator $paginator
     * @param TripFactory $tripFactory
     * @return View|PaginatedRepresentation
     *
     * @REST\QueryParam(name="limit", requirements="\d+", default="5", description="Max number of Flights per page.")
     * @REST\QueryParam(name="page", requirements="\d+", default="1", description="The page of the overview")
     *
     * @REST\View(populateDefaultVars=false, serializerGroups={"Default","trip_list"})
     */
    public function getOneWayTrip(Request $request, Paginator $paginator, TripFactory $tripFactory)
    {
        $data = \json_decode($request->getContent(), true);
        if (!$data){
            throw new BadRequestHttpException();
        }

        if (($form = $this->validateData($data, OneWayTripType::class)) !== true) {
            return View::create($form, Response::HTTP_BAD_REQUEST);
        }

        $trips = $tripFactory->createOneWayTrip($data);

        $paginator
            ->setDoctrineCollection(new ArrayCollection($trips))
            ->setRoute(new Route('get_one_way_trip', [], true))
            ->setRel('trips');

        return $paginator->getRepresentation();
    }

    /**
     * @param Request $request
     * @param Paginator $paginator
     * @param TripFactory $tripFactory
     * @return PaginatedRepresentation
     * @throws \Exception
     *
     * @REST\QueryParam(name="limit", requirements="\d+", default="5", description="Max number of Flights per page.")
     * @REST\QueryParam(name="page", requirements="\d+", default="1", description="The page of the overview")
     *
     * @REST\View(populateDefaultVars=false, serializerGroups={"Default","trip_list"})
     */
    public function getRoundTrip(Request $request, Paginator $paginator, TripFactory $tripFactory)
    {
        $data = \json_decode($request->getContent(), true);
        if (!$data){
            throw new BadRequestHttpException();
        }

        if (($form = $this->validateData($data, RoundTripType::class)) !== true) {
            return View::create($form, Response::HTTP_BAD_REQUEST);
        }

		$trips = $tripFactory->createRoundTrip($data);

        $paginator
            ->setDoctrineCollection(new ArrayCollection($trips))
            ->setRoute(new Route('get_round_trip', [], true))
            ->setRel('trips');

        return $paginator->getRepresentation();
    }
	
	/**
     * @param Request $request
     * @param Paginator $paginator
     * @param TripFactory $tripFactory
     * @return PaginatedRepresentation
     * @throws \Exception
     *
     * @REST\QueryParam(name="limit", requirements="\d+", default="5", description="Max number of Flights per page.")
     * @REST\QueryParam(name="page", requirements="\d+", default="1", description="The page of the overview")
     *
     * @REST\View(populateDefaultVars=false, serializerGroups={"Default","trip_list"})
     */
    public function getOpenJawTrip(Request $request, Paginator $paginator, TripFactory $tripFactory)
    {
        $data = \json_decode($request->getContent(), true);
        if (!$data){
            throw new BadRequestHttpException();
        }

        if (($form = $this->validateData($data, OpenJawTripType::class)) !== true) {
            return View::create($form, Response::HTTP_BAD_REQUEST);
        }

		$trips = $tripFactory->createOpenJawTrip($data);

        $paginator
            ->setDoctrineCollection(new ArrayCollection($trips))
            ->setRoute(new Route('get_open_jaw_trip', [], true))
            ->setRel('trips');

        return $paginator->getRepresentation();
    }
	
	/**
     * @param Request $request
     * @param Paginator $paginator
     * @param TripFactory $tripFactory
     * @return PaginatedRepresentation
     * @throws \Exception
     *
     * @REST\QueryParam(name="limit", requirements="\d+", default="5", description="Max number of Flights per page.")
     * @REST\QueryParam(name="page", requirements="\d+", default="1", description="The page of the overview")
     *
     * @REST\View(populateDefaultVars=false, serializerGroups={"Default","trip_list"})
     */
    public function getMultiCityTrip(Request $request, Paginator $paginator, TripFactory $tripFactory)
    {
        $data = \json_decode($request->getContent(), true);
        if (!$data){
            throw new BadRequestHttpException();
        }

        $reqFlights = $data['flights'];
        foreach ($reqFlights as $flightData){
            if (($form = $this->validateData($flightData, OneWayTripType::class)) !== true) {
                return View::create($form, Response::HTTP_BAD_REQUEST);
            }
        }
				
		$trips = $tripFactory->createMultiCityTrip($data);

        $paginator
            ->setDoctrineCollection(new ArrayCollection($trips))
            ->setRoute(new Route('get_multi_city_trip', [], true))
            ->setRel('trips');

        return $paginator->getRepresentation();
    }

    /**
     * @param array $data
     * @param $tripType
     * @return bool|\Symfony\Component\Form\FormInterface
     */
    private function validateData(array $data, $tripType)
    {
        $form = $this->createForm($tripType);
        $form->submit($data);

        if (!$form->isValid()) {
            return $form;
        }

        return true;
    }
}