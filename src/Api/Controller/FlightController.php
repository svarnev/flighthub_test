<?php

namespace App\Api\Controller;

use App\Api\Entity\Flight;
use App\Api\Form\FlightType;
use App\Api\Repository\FlightRepository;
use App\Api\Service\Paginator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as REST;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Hateoas\Configuration\Route;
use Hateoas\Representation\PaginatedRepresentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter as PARAMCONVERTER;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;


/**
 * Class FlightController
 * @package App\Api\Controller
 */
class FlightController extends FOSRestController
{
    /**
     * @param FlightRepository $flightRepository
     * @param Paginator $paginator
     *
     * @return PaginatedRepresentation
     *
     * @REST\QueryParam(name="limit", requirements="\d+", default="5", description="Max number of Flights per page.")
     * @REST\QueryParam(name="page", requirements="\d+", default="1", description="The page of the overview")
     *
     * @REST\View(populateDefaultVars=false, serializerGroups={"Default","flight_list"})
     */
    public function getFlights(FlightRepository $flightRepository, Paginator $paginator)
    {
        $flights = $flightRepository->findAll();

        $paginator
            ->setDoctrineCollection(new ArrayCollection($flights))
            ->setRoute(new Route('get_flights', [], true))
            ->setRel('flights');

        return $paginator->getRepresentation();
    }

    /**
     * @param Flight $flight
     *
     * @return Flight
     *
     * @REST\View(populateDefaultVars=false, serializerGroups={"Default","flight"})
     *
     * @PARAMCONVERTER("flight", class="App:Flight", options={"mapping": {"flight_id": "id"}})
     */
    public function getFlight(Flight $flight)
    {
        return $flight;
    }

    /**
     * @param Request $request
     * @param RouterInterface $router
     * @return mixed
     *
     * @REST\View(populateDefaultVars=false, statusCode=Response::HTTP_NO_CONTENT)
     */
    public function postFlight(Request $request, RouterInterface $router)
    {
        return $this->_processFlightForm(new Flight(), $request, $router);
    }

    /**
     * @param Flight $flight
     * @param Request $request
     * @param RouterInterface $router
     * @return mixed
     *
     * @REST\View(populateDefaultVars=false, statusCode=Response::HTTP_NO_CONTENT)
     *
     * @PARAMCONVERTER("flight", class="App:Flight", options={"mapping": {"flight_id": "id"}})
     */
    public function putFlight(Flight $flight, Request $request, RouterInterface $router)
    {
        return $this->_processFlightForm($flight, $request, $router);
    }

    /**
     * @param Flight $flight
     *
     * @REST\View(populateDefaultVars=false)
     *
     * @PARAMCONVERTER("flight", class="App:Flight", options={"mapping": {"flight_id": "id"}})
     */
    public function deleteFlight(Flight $flight)
    {
        if (!is_null($flight)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($flight);
            $em->flush();
        }
    }

    /**
     * @param Flight $flight
     * @param Request $request
     * @param RouterInterface $router
     * @return View
     */
    protected function _processFlightForm(Flight $flight, Request $request, RouterInterface $router)
    {
        $data = \json_decode($request->getContent(), true);

        $form = $this->createForm(FlightType::class, $flight);
        $form->submit($data);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($flight);
            $em->flush();

            return View::create('', Response::HTTP_NO_CONTENT, [
                'Location' => $router->generate('get_flight', ['flight_id' => $flight->getId()], Router::ABSOLUTE_URL),
            ]);
        }

        return View::create($form, Response::HTTP_BAD_REQUEST);
    }
}