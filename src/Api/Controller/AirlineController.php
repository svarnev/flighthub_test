<?php

namespace App\Api\Controller;

use App\Api\Entity\Airline;
use App\Api\Form\AirlineType;
use App\Api\Repository\AirlineRepository;
use App\Api\Service\Paginator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as REST;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Hateoas\Configuration\Route;
use Hateoas\Representation\PaginatedRepresentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter as PARAMCONVERTER;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class AirlineController
 * @package App\Api\Controller
 */
class AirlineController extends FOSRestController
{
    /**
     * @param AirlineRepository $airlineRepository
     * @param Paginator $paginator
     *
     * @return PaginatedRepresentation
     *
     * @REST\QueryParam(name="limit", requirements="\d+", default="5", description="Max number of Airlines per page.")
     * @REST\QueryParam(name="page", requirements="\d+", default="1", description="The page of the overview")
     *
     * @REST\View(populateDefaultVars=false, serializerGroups={"Default","airline_list"})
     */
    public function getAirlines(AirlineRepository $airlineRepository, Paginator $paginator)
    {
        $airlines = $airlineRepository->findAll();

        $paginator
            ->setDoctrineCollection(new ArrayCollection($airlines))
            ->setRoute(new Route('get_airlines', [], true))
            ->setRel('airlines');

        return $paginator->getRepresentation();
    }

    /**
     * @param Airline $airline
     *
     * @return Airline
     *
     * @REST\View(populateDefaultVars=false, serializerGroups={"Default","airline"})
     *
     * @PARAMCONVERTER("airline", class="App:Airline", options={"mapping": {"airline_id": "id"}})
     */
    public function getAirline(Airline $airline)
    {
        return $airline;
    }

    /**
     * @param Request $request
     * @param RouterInterface $router
     * @return mixed
     *
     * @REST\View(populateDefaultVars=false, statusCode=Response::HTTP_NO_CONTENT)
     */
    public function postAirline(Request $request, RouterInterface $router)
    {
        return $this->_processAirlineForm(new Airline(), $request, $router);
    }

    /**
     * @param Airline $airline
     * @param Request $request
     * @param RouterInterface $router
     * @return mixed
     *
     * @REST\View(populateDefaultVars=false, statusCode=Response::HTTP_NO_CONTENT)
     *
     * @PARAMCONVERTER("airline", class="App:Airline", options={"mapping": {"airline_id": "id"}})
     */
    public function putAirline(Airline $airline, Request $request, RouterInterface $router)
    {
        return $this->_processAirlineForm($airline, $request, $router);
    }

    /**
     * @param Airline $airline
     *
     * @REST\View(populateDefaultVars=false)
     *
     * @PARAMCONVERTER("airline", class="App:Airline", options={"mapping": {"airline_id": "id"}})
     */
    public function deleteAirline(Airline $airline)
    {
//        throw new MethodNotAllowedHttpException([
//            Request::METHOD_GET,
//            Request::METHOD_POST,
//            Request::METHOD_PUT,
//        ]);

        if (!is_null($airline)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($airline);
            $em->flush();
        }
    }

    /**
     * @param Airline $airline
     * @param Request $request
     * @param RouterInterface $router
     * @return View
     */
    protected function _processAirlineForm(Airline $airline, Request $request, RouterInterface $router)
    {
        $data = \json_decode($request->getContent(), true);
        $form = $this->createForm(AirlineType::class, $airline);
        $form->submit($data);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($airline);
            $em->flush();

            return View::create('', Response::HTTP_NO_CONTENT, [
                'Location' => $router->generate('get_airline', ['airline_id' => $airline->getId()], Router::ABSOLUTE_URL),
            ]);
        }
        return View::create($form, Response::HTTP_BAD_REQUEST);
    }
}