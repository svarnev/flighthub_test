<?php

namespace App\Api\Controller;

use App\Api\Entity\Airport;
use App\Api\Form\AirportType;
use App\Api\Repository\AirportRepository;
use App\Api\Service\Paginator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as REST;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Hateoas\Configuration\Route;
use Hateoas\Representation\PaginatedRepresentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter as PARAMCONVERTER;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;


/**
 * Class AirportController
 * @package App\Api\Controller
 */
class AirportController extends FOSRestController
{
    /**
     * @param AirportRepository $airportRepository
     * @param Paginator $paginator
     *
     * @return PaginatedRepresentation
     *
     * @REST\QueryParam(name="limit", requirements="\d+", default="5", description="Max number of Airports per page.")
     * @REST\QueryParam(name="page", requirements="\d+", default="1", description="The page of the overview")
     *
     * @REST\View(populateDefaultVars=false, serializerGroups={"Default","airport_list"})
     */
    public function getAirports(AirportRepository $airportRepository, Paginator $paginator)
    {
        $airports = $airportRepository->findAll();

        $paginator
            ->setDoctrineCollection(new ArrayCollection($airports))
            ->setRoute(new Route('get_airports', [], true))
            ->setRel('airports');

        return $paginator->getRepresentation();
    }

    /**
     * @param Airport $airport
     *
     * @return Airport
     *
     * @REST\View(populateDefaultVars=false, serializerGroups={"Default","airport"})
     *
     * @PARAMCONVERTER("airport", class="App:Airport", options={"mapping": {"airport_id": "id"}})
     */
    public function getAirport(Airport $airport)
    {
        return $airport;
    }

    /**
     * @param Request $request
     * @param RouterInterface $router
     * @return mixed
     *
     * @REST\View(populateDefaultVars=false, statusCode=Response::HTTP_NO_CONTENT)
     */
    public function postAirport(Request $request, RouterInterface $router)
    {
        return $this->_processAirportForm(new Airport(), $request, $router);
    }

    /**
     * @param Airport $airport
     * @param Request $request
     * @param RouterInterface $router
     * @return mixed
     *
     * @REST\View(populateDefaultVars=false, statusCode=Response::HTTP_NO_CONTENT)
     *
     * @PARAMCONVERTER("airport", class="App:Airport", options={"mapping": {"airport_id": "id"}})
     */
    public function putAirport(Airport $airport, Request $request, RouterInterface $router)
    {
        return $this->_processAirportForm($airport, $request, $router);
    }

    /**
     * @param Airport $airport
     *
     * @REST\View(populateDefaultVars=false)
     *
     * @PARAMCONVERTER("airport", class="App:Airport", options={"mapping": {"airport_id": "id"}})
     */
    public function deleteAirport(Airport $airport)
    {
//        throw new MethodNotAllowedHttpException([
//            Request::METHOD_GET,
//            Request::METHOD_POST,
//            Request::METHOD_PUT,
//        ]);

        if (!is_null($airport)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($airport);
            $em->flush();
        }
    }

    /**
     * @param Airport $airport
     * @param Request $request
     * @param RouterInterface $router
     * @return View
     */
    protected function _processAirportForm(Airport $airport, Request $request, RouterInterface $router)
    {
        $data = \json_decode($request->getContent(), true);
        $form = $this->createForm(AirportType::class, $airport);
        $form->submit($data);


        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($airport);
            $em->flush();

            return View::create('', Response::HTTP_NO_CONTENT, [
                'Location' => $router->generate('get_airport', ['airport_id' => $airport->getId()], Router::ABSOLUTE_URL),
            ]);
        }
        return View::create($form, Response::HTTP_BAD_REQUEST);
    }
}