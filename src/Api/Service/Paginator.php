<?php

namespace App\Api\Service;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Hateoas\Configuration\Route;
use Hateoas\Representation\CollectionRepresentation;
use Hateoas\Representation\Factory\PagerfantaFactory;
use Pagerfanta\Adapter\DoctrineCollectionAdapter;
use Pagerfanta\Pagerfanta;

/**
 * Class Paginator.
 */
class Paginator
{
    /**
     * @var ParamFetcherInterface
     */
    protected $paramFetcher;

    /**
     * @var
     */
    protected $route;

    /**
     * @var
     */
    protected $doctrineCollection;

    /**
     * @var
     */
    protected $rel;

    /**
     * @var
     */
    protected $xmlRel;

    /**
     * Paginator constructor.
     *
     * @param ParamFetcherInterface $paramFetcher
     */
    public function __construct(ParamFetcherInterface $paramFetcher)
    {
        $this->paramFetcher = $paramFetcher;
    }

    /**
     * @return \Hateoas\Representation\PaginatedRepresentation
     */
    public function getRepresentation()
    {
        $adapter = new DoctrineCollectionAdapter($this->getDoctrineCollection());
        $pager = new Pagerfanta($adapter);
        $pager
            ->setMaxPerPage($this->paramFetcher->get('limit'))
            ->setCurrentPage($this->paramFetcher->get('page'));

        $pagerFactory = new PagerfantaFactory();
        $paginatedCollection = $pagerFactory->createRepresentation(
            $pager,
            $this->route,
            new CollectionRepresentation(
                $pager->getCurrentPageResults(),
                $this->getRel(),
                $this->getXmlRel()));

        return $paginatedCollection;
    }

    /**
     * @param ArrayCollection $doctrineCollection
     *
     * @return $this
     */
    public function setDoctrineCollection($doctrineCollection)
    {
        $this->doctrineCollection = $doctrineCollection;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDoctrineCollection()
    {
        return $this->doctrineCollection;
    }

    /**
     * @param Route $route
     *
     * @return $this
     */
    public function setRoute(Route $route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param $rel
     *
     * @return $this
     */
    public function setRel($rel)
    {
        $this->rel = $rel;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRel()
    {
        return $this->rel;
    }

    /**
     * @param $xmlRel
     *
     * @return $this
     */
    public function setXmlRel($xmlRel)
    {
        $this->xmlRel = $xmlRel;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getXmlRel()
    {
        return $this->xmlRel ?: $this->rel;
    }
}
