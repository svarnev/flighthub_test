<?php

namespace App\Api\Service;

use App\Api\Entity\Flight;

class DateTimeService
{
    /**
     * @param Flight $flight
     * @return bool
     * @throws \Exception
     */
    public function isInPeriodLimits(Flight $flight)
    {
        $timeZone = new \DateTimeZone($flight->getDepartureAirport()->getTimezone());
        $flightDate = $flight->getDepartureTime();
        $nowDate = new \DateTime('NOW',$timeZone);

        if ($flightDate < $nowDate){
            return false;
        }

        if ($flightDate > $nowDate->modify('+365 days')){
            return false;
        }

        return true;
    }

    /**
     * @param \DateTimeInterface $flightTime
     * @param string $flightDate
     * @param string $flightTimeZone
     * @return \DateTime
     * @throws \Exception
     */
	public function modifyDate(\DateTimeInterface $flightTime, string $flightDate, string $flightTimeZone)
    {
		$strTime = ($flightTime->format("H:i"));
		$timeZone = new \DateTimeZone($flightTimeZone);
		return new \DateTime($flightDate.' '.$strTime, $timeZone);
	}
}