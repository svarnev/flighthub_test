<?php

namespace App\Api\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

abstract class ItemToCodeTransformer implements DataTransformerInterface
{
    private $entityManager;
    private $entity;

    public function __construct(EntityManagerInterface $entityManager, string $entity)
    {
        $this->entityManager = $entityManager;
        $this->entity = $entity;
    }

    public function transform($item)
    {
        if (is_null($item)) {
            return '';
        }

        return $this->getCode($item);
    }

    public function reverseTransform($code)
    {
        if (!$code) {
            return;
        }

        $item = $this->entityManager->getRepository($this->entity)->findOneBy($this->getCriteria($code));

        if (is_null($item)) {
            throw new TransformationFailedException(sprintf(
                'An item with code "%s" does not exist!',
                $code
            ));
        }

        return $item;
    }

    public function getCode($item)
    {
        return $item->getCode();
    }

    public function getCriteria($code)
    {
        return ['code' => $code];
    }
}