<?php

namespace App\Api\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use App\Api\Entity\Airport;

class AirportToCodeTransformer extends ItemToCodeTransformer
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, Airport::class);
    }
}