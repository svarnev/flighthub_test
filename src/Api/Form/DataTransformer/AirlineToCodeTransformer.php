<?php

namespace App\Api\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use App\Api\Entity\Airline;

class AirlineToCodeTransformer extends ItemToCodeTransformer
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, Airline::class);
    }
}