<?php

namespace App\Api\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class OneWayTripType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('departure_location', TextType::class, [
                'constraints' => [
                    new NotBlank()
                 ],
            ])
            ->add('arrival_location', TextType::class, [
                'constraints' => [
                    new NotBlank()
                ],
            ])
            ->add('departure_date', DateTimeType::class, [
                'widget'=>'single_text',
                'format'=>'yyyy-MM-dd'
            ])
            ->add('airline_code',  TextType::class, [
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'csrf_protection' => false
        ]);
    }
}
