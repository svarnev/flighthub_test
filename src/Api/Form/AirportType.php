<?php

namespace App\Api\Form;

use App\Api\Entity\Airport;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AirportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code')
            ->add('city_code')
            ->add('name')
            ->add('city')
            ->add('country_code')
            ->add('region_code')
            ->add('latitude')
            ->add('longitude')
            ->add('timezone')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Airport::class,
            'csrf_protection' => false
        ]);
    }
}
