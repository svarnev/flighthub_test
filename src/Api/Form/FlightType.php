<?php

namespace App\Api\Form;

use App\Api\Entity\Flight;
use App\Api\Form\DataTransformer\AirlineToCodeTransformer;
use App\Api\Form\DataTransformer\AirportToCodeTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FlightType extends AbstractType
{
    private $airlineToCodeTransformer;
    private $airportToCodeTransformer;

    public function __construct(AirlineToCodeTransformer $airlineToCodeTransformer, AirportToCodeTransformer $airportToCodeTransformer)
    {
        $this->airlineToCodeTransformer = $airlineToCodeTransformer;
        $this->airportToCodeTransformer = $airportToCodeTransformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number')
            ->add('departure_time', DateTimeType::class, [
                'widget'=>'single_text',
                'format'=>'HH:mm'
            ])
            ->add('arrival_time', DateTimeType::class, [
                'widget'=>'single_text',
                'format'=>'HH:mm'
            ])
            ->add('price')
            ->add('airline',  TextType::class)
            ->add('departure_airport', TextType::class)
            ->add('arrival_airport', TextType::class)
        ;

        $builder->get('airline')->addModelTransformer($this->airlineToCodeTransformer);
        $builder->get('departure_airport')->addModelTransformer($this->airportToCodeTransformer);
        $builder->get('arrival_airport')->addModelTransformer($this->airportToCodeTransformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Flight::class,
            'csrf_protection' => false
        ]);
    }
}
