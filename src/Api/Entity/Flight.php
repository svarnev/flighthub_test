<?php

namespace App\Api\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as ASSERT;
use Gedmo\Mapping\Annotation as GEDMO;
use JMS\Serializer\Annotation as SERIALIZER;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\FlightRepository")
 *
 * @SERIALIZER\ExclusionPolicy("all")
 */
class Flight
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "flight", "flight_list"
     * })
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\Airline", inversedBy="flights")
     * @ORM\JoinColumn(nullable=false)
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "flight", "flight_list", "trip_list"
     * })
     */
    private $airline;

    /**
     * @ORM\Column(type="string", length=10)
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "flight", "flight_list", "trip_list"
     * })
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\Airport", inversedBy="flights")
     * @ORM\JoinColumn(nullable=false)
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "flight", "flight_list", "trip_list"
     * })
     */
    private $departureAirport;

    /**
     * @var \DateTime $departureTime
     *
     * @GEDMO\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     *
     * @ASSERT\DateTime(format="c")
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "flight", "flight_list", "trip_list"
     * })
     *
     */
    private $departureTime;

    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\Airport")
     * @ORM\JoinColumn(nullable=false)
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "flight", "flight_list", "trip_list"
     * })
     */
    private $arrivalAirport;

    /**
     * @var \DateTime $arrivalTime
     *
     * @GEDMO\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     *
     * @ASSERT\DateTime(format="c")
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "flight", "flight_list", "trip_list"
     * })
     */
    private $arrivalTime;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "flight", "flight_list", "trip_list"
     * })
     */
    private $price;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAirline(): ?Airline
    {
        return $this->airline;
    }

    public function setAirline(?Airline $airline): self
    {
        $this->airline = $airline;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getDepartureAirport(): ?Airport
    {
        return $this->departureAirport;
    }

    public function setDepartureAirport(?Airport $departureAirport): self
    {
        $this->departureAirport = $departureAirport;

        return $this;
    }

    public function getDepartureTime(): ?\DateTimeInterface
    {
        return $this->departureTime;
    }

    public function setDepartureTime(\DateTimeInterface $departureTime): self
    {
        $this->departureTime = $departureTime;

        return $this;
    }

    public function getArrivalAirport(): ?Airport
    {
        return $this->arrivalAirport;
    }

    public function setArrivalAirport(?Airport $arrivalAirport): self
    {
        $this->arrivalAirport = $arrivalAirport;

        return $this;
    }

    public function getArrivalTime(): ?\DateTimeInterface
    {
        return $this->arrivalTime;
    }

    public function setArrivalTime(\DateTimeInterface $arrivalTime): self
    {
        $this->arrivalTime = $arrivalTime;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(?string $price): self
    {
        $this->price = $price;

        return $this;
    }
}
