<?php

namespace App\Api\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as SERIALIZER;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\AirportRepository")
 *
 * @SERIALIZER\ExclusionPolicy("all")
 */
class Airport
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airport", "airport_list"
     * })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airport", "airport_list", "flight", "flight_list", "trip_list"
     * })
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=5)
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airport", "airport_list", "flight", "flight_list", "trip_list"
     * })
     */
    private $cityCode;

    /**
     * @ORM\Column(type="string", length=100)
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airport", "airport_list", "flight", "flight_list", "trip_list"
     * })
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=40)
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airport", "airport_list", "flight", "flight_list", "trip_list"
     * })
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=5)
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airport", "airport_list", "flight", "flight_list", "trip_list"
     * })
     */
    private $countryCode;

    /**
     * @ORM\Column(type="string", length=5)
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airport", "airport_list", "flight", "flight_list", "trip_list"
     * })
     */
    private $regionCode;

    /**
     * @ORM\Column(type="float")
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airport", "airport_list", "flight", "flight_list", "trip_list"
     * })
     */
    private $latitude;

    /**
     * @ORM\Column(type="float")
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airport", "airport_list", "flight", "flight_list", "trip_list"
     * })
     */
    private $longitude;

    /**
     * @ORM\Column(type="string", length=50)
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airport", "airport_list", "flight", "flight_list", "trip_list"
     * })
     */
    private $timezone;

    /**
     * @ORM\OneToMany(targetEntity="App\Api\Entity\Flight", mappedBy="departureAirport")
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airport", "airport_list"
     * })
     */
    private $flights;

    public function __construct()
    {
        $this->flights = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getCityCode(): ?string
    {
        return $this->cityCode;
    }

    public function setCityCode(string $cityCode): self
    {
        $this->cityCode = $cityCode;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    public function setCountryCode(string $countryCode): self
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    public function getRegionCode(): ?string
    {
        return $this->regionCode;
    }

    public function setRegionCode(string $regionCode): self
    {
        $this->regionCode = $regionCode;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    public function setTimezone(string $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * @return Collection|Flight[]
     */
    public function getFlights(): Collection
    {
        return $this->flights;
    }

    public function addFlight(Flight $flight): self
    {
        if (!$this->flights->contains($flight)) {
            $this->flights[] = $flight;
            $flight->setDepartureAirport($this);
        }

        return $this;
    }

    public function removeFlight(Flight $flight): self
    {
        if ($this->flights->contains($flight)) {
            $this->flights->removeElement($flight);
            // set the owning side to null (unless already changed)
            if ($flight->getDepartureAirport() === $this) {
                $flight->setDepartureAirport(null);
            }
        }

        return $this;
    }
}
