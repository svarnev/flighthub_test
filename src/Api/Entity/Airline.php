<?php

namespace App\Api\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as SERIALIZER;
use Symfony\Component\Validator\Constraints as CONSTRAINT;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity AS UNIQUEENTITY;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\AirlineRepository")
 * @UNIQUEENTITY("code", groups={}, message="This code already exist")
 *
 * @SERIALIZER\ExclusionPolicy("all")
 */
class Airline
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airline", "airline_list"
     * })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     * @CONSTRAINT\NotBlank(
     *     message = "Code should not be blank"
     * )
     * @CONSTRAINT\Length(
     *      min = 2,
     *      max = 5,
     *      minMessage = "Code should have {{ limit }} characters or more",
     *      maxMessage = "Code should have {{ limit }} characters or less"
     * )
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airline", "airline_list", "flight", "flight_list", "trip_list"
     * })
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=70)
     *
     * @CONSTRAINT\NotBlank(
     *     message = "Name should not be blank"
     * )
     * @CONSTRAINT\Length(
     *      min = 3,
     *      max = 70,
     *      minMessage = "Name should have {{ limit }} characters or more",
     *      maxMessage = "Name should have {{ limit }} characters or less"
     * )
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airline", "airline_list", "flight", "flight_list", "trip_list"
     * })
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Api\Entity\Flight", mappedBy="airline")
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airline", "airline_list"
     * })
     */
    private $flights;

    public function __construct()
    {
        $this->flights = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Flight[]
     */
    public function getFlights(): Collection
    {
        return $this->flights;
    }

    public function addFlight(Flight $flight): self
    {
        if (!$this->flights->contains($flight)) {
            $this->flights[] = $flight;
            $flight->setAirline($this);
        }

        return $this;
    }

    public function removeFlight(Flight $flight): self
    {
        if ($this->flights->contains($flight)) {
            $this->flights->removeElement($flight);
            // set the owning side to null (unless already changed)
            if ($flight->getAirline() === $this) {
                $flight->setAirline(null);
            }
        }

        return $this;
    }
}
