<?php

namespace App\Api\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as SERIALIZER;

/**
 * 
 *
 * @SERIALIZER\ExclusionPolicy("all")
 */
class Trip
{
    /**
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airline", "airline_list", "flight", "flight_list", "trip_list"
     * })
     */
    private $flights;

    /**
     *
     * @SERIALIZER\Expose
     * @SERIALIZER\Groups({
     *     "airline", "airline_list", "flight", "flight_list", "trip_list"
     * })
     */
    private $totalPrice;

   
    public function __construct()
    {
        $this->flights = new ArrayCollection();
		$this->totalPrice = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTotalPrice(): float
    {
        return $this->totalPrice;
    }

    public function setTotalPrice(float $totalPrice): self
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

   
    /**
     * @return Collection|Flight[]
     */
    public function getFlights(): Collection
    {
        return $this->flights;
    }

    public function addFlight(Flight $flight): self
    {
        if (!$this->flights->contains($flight)) {
            $this->flights[] = $flight;
        }

        return $this;
    }

    public function removeFlight(Flight $flight): self
    {
        if ($this->flights->contains($flight)) {
            $this->flights->removeElement($flight);
        }

        return $this;
    }
}
