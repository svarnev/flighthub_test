<?php

namespace App\Web\Controller;

use App\Web\Manager\FlighthubApiManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

    public function index(FlighthubApiManager $flighthubApiManager)
    {
        $reponse = $flighthubApiManager->getAirlines();

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }
}
