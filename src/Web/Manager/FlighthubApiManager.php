<?php

namespace App\Web\Manager;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

class FlighthubApiManager
{
    protected $flighthubApiClient;
    protected $serializer;
    protected $logger;
    protected $loggerUpdateSubscription;

    /**
     * FlighthubApiManager constructor.
     * @param ClientInterface $flighthubApiClient
     * @param SerializerInterface $serializer
     * @param LoggerInterface $logger
     * @param LoggerInterface $loggerUpdateSubscription
     */
    public function __construct(ClientInterface $flighthubApiClient, SerializerInterface $serializer, LoggerInterface $logger, LoggerInterface $loggerUpdateSubscription)
    {
        $this->flighthubApiClient = $flighthubApiClient;
        $this->serializer = $serializer;
        $this->logger = $logger;
        $this->loggerUpdateSubscription = $loggerUpdateSubscription;
    }

    /**
     * @return mixed|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAirlines()
    {
        try {

            $response = $this->flighthubApiClient->request('GET', '/api/airlines');
        } catch (ClientException $e) {
            return null;
        } catch (\Exception $e) {


            $reason = "Unable to get airlines from Flighthub API";
            $this->logger->error($e->getMessage(), ['reason' => $reason]);
            throw new \Exception($reason);
        }

        return \json_decode($response->getBody());
    }
}