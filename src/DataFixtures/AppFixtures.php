<?php

namespace App\DataFixtures;

use App\Api\Entity\Airport;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Api\Entity\Flight;
use App\Api\Entity\Airline;
use Symfony\Component\Validator\Constraints\DateTime;

class AppFixtures extends Fixture
{
    protected $airlines = [
        ['Aegean Airlines','A3'],
        ['Aer Arann','RE'],
        ['Aer Lingus','EI'],
        ['Aeroflot Russian Airlines','SU'],
        ['Aerolineas Argentinas','AR'],
        ['Aeromexico','AM'],
        ['Air Algerie','AH'],
        ['Air Astana','KC'],
        ['Air Canada','AC'],
        ['Air China','CA'],
        ['Air Europa','UX'],
        ['Air France','AF'],
        ['Air India','AI'],
        ['Air Namibia','SW'],
        ['Air New Zealand','NZ'],
        ['Air Seychelles','HM'],
        ['Air Tahiti','VT'],
        ['Air Zimbabwe','UM'],
        ['Alaska Airlines','AS'],
        ['Alitalia','AZ'],
        ['All Nippon Airways','NH'],
        ['American Airlines','AA'],
        ['Arik Air','W3'],
        ['Asiana Airlines','OZ'],
        ['Atlantic Airways','RC'],
        ['Aurigny','GR'],
        ['Austrian Airlines','OS'],
        ['Avianca','AV'],
        ['Azerbaijan Hava Yollary','J2'],
        ['Bangkok Airways','PG'],
        ['Blue1','KF'],
        ['British Airways','BA'],
        ['Brussels Airlines','SN'],
        ['Bulgaria Air','FB'],
        ['Cathay Pacific','CX'],
        ['Czech Airlines','OK'],
        ['China Airlines','CI'],
        ['China Eastern Airlines','MU'],
        ['China Southern Airlines','CZ'],
        ['Croatia Airlines','OU'],
        ['Cyprus Airways','CY'],
        ['Delta Air Lines','DL'],
        ['Eastern Airways','T3'],
        ['Egyptair','MS'],
        ['El Al Israel Airlines','LY'],
        ['Emirates','EK'],
        ['Estonian Air','OV'],
        ['Ethiopian Airlines','ET'],
        ['Etihad Airways','EY'],
        ['Eva Air','BR'],
        ['Finnair','AY'],
        ['Flybe','BE'],
        ['Garuda Indonesia','GA'],
        ['Gulf Air','GF'],
        ['HAHN Air','HR'],
        ['Hong Kong Airlines','HX'],
        ['Iberia','IB'],
        ['Icelandair','FI'],
        ['Japan Airlines','JL'],
        ['Jet Airways','9W'],
        ['Kenya Airways','KQ'],
        ['KLM Royal Dutch Airlines','KL'],
        ['Korean Air','KE'],
        ['Kuwait Airways','KU'],
        ['LAN Colombia','LA'],
        ['LOT - Polish Airlines','LO'],
        ['Lufthansa','LH'],
        ['Luxair','LG'],
        ['Malaysia Airlines','MH'],
        ['Middle East Airlines','ME'],
        ['Meridiana','IG'],
        ['Mexicana','MX'],
        ['Northwest Airlines','NW'],
        ['Norwegian Air Shuttle','DY'],
        ['Olympic Air','OA'],
        ['Oman Air','WY'],
        ['Rossiya-Russia Airlines','FV'],
        ['Qantas Airways','QF'],
        ['Qatar Airways','QR'],
        ['Royal Air Maroc','AT'],
        ['Royal Brunei Airlines','BI'],
        ['Royal Jordanian','RJ'],
        ['Siberia Airlines','S7'],
        ['Saudi Arabian Airlines','SV'],
        ['Scandinavian Airlines System (SAS)','SK'],
        ['Singapore Airlines','SQ'],
        ['South African Airways','SA'],
        ['Spanair','JK'],
        ['SriLankan Airlines','UL'],
        ['SWISS International Air Lines','LX'],
        ['TAM Airlines','JJ'],
        ['TAP Portugal','TP'],
        ['Tarom','RO'],
        ['Thai Airways International','TG'],
        ['Transaero Airlines','UN'],
        ['Tunisair','TU'],
        ['Turkish Airlines','TK'],
        ['Ukraine International Airlines','PS'],
        ['United Airlines','UA'],
        ['US Airways','US'],
        ['Uzbekistan Airways','HY'],
        ['Vietnam Airlines','VN'],
        ['Virgin Atlantic Airways','VS'],
        ['Xiamen Airlines','MF']
    ];

    protected $airports = [
		['YUL','YMQ','Pierre Elliott Trudeau International','Montreal','CA','QC',45.457714,-73.749908,'America/Montreal'],
        ['YVR','YVR','Vancouver International','Vancouver','CA','BC',49.194698,-123.179192,'America/Vancouver'],
		['TOR','TOR','Toronto International','Toronto','CA','BC',49.194698,-123.179192,'America/Toronto'],
		['CAL','CAL','Calgary International','Calgary','CA','BC',49.194698,-123.179192,'America/Edmonton'],
		['QBC','QBC','Quebec International','Quebec','CA','BC',49.194698,-123.179192,'America/Montreal']
	];
	
	protected $flights = [
		['AC','301','YUL','07:35','YVR','10:05',273.23],
		['AF','302','YUL','08:35','YVR','11:05',273.23],
        ['AC','303','YVR','11:30','YUL','19:11',220.63],
		['AF','304','YVR','12:30','YUL','20:11',220.63],
		['AC','305','TOR','19:30','YUL','21:45',240.63],
		['AC','306','TOR','20:30','YUL','22:45',240.63],
		['AC','307','TOR','20:30','CAL','21:45',210.63],
		['AC','308','TOR','21:30','CAL','22:45',210.63],
		['AC','309','CAL','21:30','QBC','22:45',245.63],
		['AC','310','CAL','22:30','QBC','23:45',245.63],
		['AC','311','QBC','23:00','YVR','23:45',264.63],
		['AC','312','QBC','23:10','YVR','23:55',264.63],
		['AC','313','YVR','10:00','TOR','14:45',300.63],
		['AC','314','YVR','13:05','TOR','15:45',300.63]
	];
	

    public function load(ObjectManager $manager)
    {
        foreach ($this->airlines as $airlineItem)
        {
            $airline = new Airline();
            $airline
                ->setName($airlineItem[0])
                ->setCode($airlineItem[1]);

            $manager->persist($airline);
        }

        foreach ($this->airports as $airportItem)
        {
            $airport = new Airport();
            $airport
				->setCode($airportItem[0])
                ->setCityCode($airportItem[1])
                ->setName($airportItem[2])
				->setCity($airportItem[3])
				->setCountryCode($airportItem[4])
                ->setRegionCode($airportItem[5])
                ->setLatitude($airportItem[6])
				->setLongitude($airportItem[7])
				->setTimezone($airportItem[8]);

            $manager->persist($airport);
        }
		
		$manager->flush();
		
		$airlineRep = $manager->getRepository(Airline::class); 
		$airportRep = $manager->getRepository(Airport::class);
		foreach ($this->flights as $flightItem)
        {
            $airline = $airlineRep->findOneBy(['code' => $flightItem[0]]);
			$airportDep = $airportRep->findOneBy(['code' => $flightItem[2]]);
			$airportArr = $airportRep->findOneBy(['code' => $flightItem[4]]);
			$flight = new Flight();
            $flight
				->setAirline($airline)
                ->setNumber($flightItem[1])
                ->setDepartureAirport($airportDep)
				->setDepartureTime(new \DateTime($flightItem[3]))
				->setArrivalAirport($airportArr)
                ->setArrivalTime(new \DateTime($flightItem[5]))
                ->setPrice($flightItem[6]);

            $manager->persist($flight);
        }

        $manager->flush();
    }
}
